# Run proxy with Debian

## Installing prerequisites

Run those commands as root to install dependencies:

```bash
sudo -i
apt-get update
apt-get -y install apt-transport-https lsb-release curl gnupg wget unzip
curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
echo 'deb https://deb.nodesource.com/node_10.x stretch  main' > /etc/apt/sources.list.d/nodesource.list
apt-get update
apt-get install -y nodejs make gcc libssl-dev g++
exit
```

## Installing SoftHSM

Run those commands as root to install SoftHSM:

```bash
sudo -i
export SOFT_HSM_VERSION=2.5.0
cd /usr/src && \
  wget https://dist.opendnssec.org/source/softhsm-${SOFT_HSM_VERSION}.tar.gz && \
  tar -xzf softhsm-${SOFT_HSM_VERSION}.tar.gz && \
  cd softhsm-${SOFT_HSM_VERSION} && \
  ./configure --disable-gost && \
  make && \
  make install && \
  rm -rf softhsm-${SOFT_HSM_VERSION}.tar.gz softhsm-${SOFT_HSM_VERSION}
exit
```

## Adding a dedicated user for the proxy

Run those commands as root to create a standard user:

```bash
sudo -i
mkdir /opt/lgo-proxy
cd /opt/lgo-proxy
useradd -m lgo-proxy
chown lgo-proxy /opt/lgo-proxy
```

## Installing lgo-proxy

Adapt `<you_pin>` variable then run those commands as lgo-proxy to install the proxy:

```bash
sudo -su lgo-proxy
export TARGET_ENV=production
export PROXY_VERSION=1.0.4
wget https://gitlab.com/lgo_public/lgo-proxy-${TARGET_ENV}/-/archive/v${PROXY_VERSION}/lgo-proxy-${TARGET_ENV}-v${PROXY_VERSION}.zip
unzip lgo-proxy-${TARGET_ENV}-v${PROXY_VERSION}.zip
cd lgo-proxy-${TARGET_ENV}-v${PROXY_VERSION}
npm install --production
LGO_SIGNER_LIBRARY_PATH=/usr/local/lib/softhsm/libsofthsm2.so LGO_SIGNER_PIN=<your_pin> ./docker-entrypoint.sh init
exit
```

## Get the api key

You need to get your API access key from LGO Markets.

## Creating a systemd service

Adapt `<you_pin>` and `<your_access_key>` variables then run those commands as root to create a systemd service:

```bash
sudo -i
export TARGET_ENV=production
export PROXY_VERSION=1.0.4
echo "
[Unit]
Description=lgo-proxy LGO App
After=network-online.target

[Service]
User=lgo-proxy
Type=simple
ExecStart=/opt/lgo-proxy/lgo-proxy-${TARGET_ENV}-v${PROXY_VERSION}/docker-entrypoint.sh start
Environment="LGO_SIGNER_LIBRARY_PATH=/usr/local/lib/softhsm/libsofthsm2.so"
Environment="LGO_SIGNER_PIN=<your_pin>"
Environment="LGO_ACCESS_KEY=<your_access_key>"
Restart=always
WorkingDirectory=/opt/lgo-proxy/lgo-proxy-${TARGET_ENV}-v${PROXY_VERSION}

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/lgo-proxy.service
systemctl daemon-reload
```

## Starting lgo-proxy proxy

```bash
systemctl start lgo-proxy
systemctl status lgo-proxy
```
